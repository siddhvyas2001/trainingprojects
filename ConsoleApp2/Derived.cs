﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2;


namespace ConsoleApp2
{
    public class Derived:Super
    {
        Super derivedObj = new Super();

        internal void Printer()
        {
            Console.WriteLine(derivedObj.name);
            Console.WriteLine(derivedObj.college);
            Console.WriteLine(derivedObj.company);
        }
    }
}
