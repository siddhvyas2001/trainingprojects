﻿using Enumerations.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerations.Repository
{
    internal interface Billable
    {
        void GenerateInvoice(InvoiceOptions invoiceOptions);
    }
}
