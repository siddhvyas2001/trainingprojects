﻿using Enumerations.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerations.Repository
{
    internal interface OrderRepository
    {
        void Add(Product product);
        void Update(Product product);
        void Delete(Product product);
        Product[] GetProducts();
    }
}
