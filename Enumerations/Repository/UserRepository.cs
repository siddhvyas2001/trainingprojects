﻿using Enumerations.Constants;
using Enumerations.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerations.Repository
{
    internal class UserRepository : OrderRepository, Billable
    {
        Product[] products = new Product[20];
        int counter = 0;
        public void Add(Product product)
        {
            products[counter++] = product;
        }

        public void Delete(Product product)
        {
            throw new NotImplementedException();
        }

        public void GenerateInvoice(InvoiceOptions invoiceOptions)
        {
            int totalAmount = 0;
            foreach(Product item in products)
            {
                if (item == null) break;
                int amount = item.Price * item.Quantity;
                totalAmount += amount;
            }
            Console.WriteLine($"Your bill is  : {totalAmount}");
            switch (invoiceOptions)
            {
                case InvoiceOptions.Email:
                    Console.WriteLine("Invoice Emailed.");
                    break;
                case InvoiceOptions.PDF:
                    Console.WriteLine("Invoice PDF generated.");
                    break;
                case InvoiceOptions.SMS:
                    Console.WriteLine("Invoice sent to mobile number.");
                    break;
                case InvoiceOptions.Print:
                    Console.WriteLine("Invoice Printed.");
                    break;
            }
        }

        public Product[] GetProducts()
        {
            return products;
        }

        public void Update(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
