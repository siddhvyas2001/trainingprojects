﻿// See https://aka.ms/new-console-template for more information
using Enumerations.Constants;
using Enumerations.Model;
using Enumerations.Repository;

Console.WriteLine("Hello, World!");

UserRepository userRepository = new UserRepository();

Product product = new Product() { Name = "Lenovo", Price = 50000, Quantity = 2 };
userRepository.Add(product);

InvoiceOptions invoiceOptions = (InvoiceOptions)Convert.ToInt32(Console.ReadLine());
userRepository.GenerateInvoice(invoiceOptions);

