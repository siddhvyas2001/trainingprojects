﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerations.Constants
{
    internal enum InvoiceOptions
    {
        Email = 1,
        PDF, 
        SMS,
        Print
    }
}
