﻿// See https://aka.ms/new-console-template for more information
using FileHandling.Model;
using FileHandling.Repository;

Console.WriteLine("Hello, World!");

UserRepo userRepo = new UserRepo();
IntUserRepo intUserRepo = (IntUserRepo)userRepo;

Console.WriteLine("Welcome");
User user = new User();
Console.WriteLine("Id : ");
user.Id = int.Parse(Console.ReadLine());
Console.WriteLine("Name : ");
user.Name = Console.ReadLine();
Console.WriteLine("City : ");
user.City = Console.ReadLine();

bool isRegistered = intUserRepo.RegisterUser(user);
//bool isRegistered =  intUserRepo.RegisterUser(new User() { Id = 1, Name = "user1", City = "Mumbai"});
if (isRegistered)
{
    Console.WriteLine("Registration  Successful");
}
else
{
    Console.WriteLine("Registration Unsuccessful");
}

Console.WriteLine();
List<string> userContent = userRepo.ReadFromFile("userDatabase.txt");
foreach(string filedata in userContent)
{
    Console.WriteLine(filedata);
}
//isRegistered = intUserRepo.RegisterUser(new User() { Id = 2, Name = "user2", City = "Rajnandgaon" });

//if (isRegistered)
//{
//    Console.WriteLine("Registration  Successful");
//}
//else
//{
//    Console.WriteLine("Registration Unsuccessful");
//}