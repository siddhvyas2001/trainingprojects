﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepo : IntUserRepo, IntFile
    {
        List<User> users = new List<User>();

        public bool DoesUserExist(string userName, string fileName)
        {
            bool exists = false;
            using(StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    string[] splitValues = rowLine.Split(",");
                    if (splitValues[1] == userName)
                    {
                        exists = true;
                    }
                }  
            }
            return exists;
        }

        public List<string> ReadFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using(StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            
            if (DoesUserExist(user.Name, fileName))
            {
                return false;
            }
            else
            {
                WriteToFile(user, fileName);
                return true;
            }
        }

        public void WriteToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))//append
            {
                //sw.Write($"Id : {user.Id} Name : {user.Name} City : {user.City}");
                sw.Write($"{user}");
            }
        }
    }
}
