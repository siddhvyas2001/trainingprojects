﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal interface IntFile
    {
        void WriteToFile(User user, string fileName);

        List<string> ReadFromFile(string fileName);
        bool DoesUserExist(string userName, string fileName);
    }
}
